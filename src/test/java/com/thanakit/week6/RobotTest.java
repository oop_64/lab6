package com.thanakit.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess(){
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getname());
        assertEquals('R', robot.getsymbol());
        assertEquals(10, robot.getx());
        assertEquals(11, robot.gety());
    }
    @Test
    public void shouldCreateRobotSuccess2(){
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getname());
        assertEquals('R', robot.getsymbol());
        assertEquals(0, robot.getx());
        assertEquals(0, robot.gety());
    }

    @Test
    public void shouldRobotUp() {
        Robot robot = new Robot("Robot", 'R', 10, 5);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(4, robot.gety());
    }

    @Test
    public void shouldRobotUp5step() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(5, robot.gety());
    }

    @Test
    public void shouldRobotdown5step() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(15, robot.gety());
    }
    @Test
    public void shouldRobotleft5step() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getx());
    }
    
    @Test
    public void shouldRobotright5step() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(15, robot.getx());
    }

    @Test
    public void shouldRobotUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_min);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_min, robot.gety());
    }

    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.gety());
    }

    @Test
    public void shouldRobotDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_max);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_max, robot.gety());
    }

    @Test
    public void shouldRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_max - 1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_max, robot.gety());
    }

    @Test
    public void shouldRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(9, robot.getx());
    }

    @Test
    public void shouldRobotLeftAtFail() {
        Robot robot = new Robot("Robot", 'R', Robot.X_min, 10);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_min, robot.getx());
    }

    @Test
    public void shouldRobotLeftBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_min + 1, 10);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(Robot.X_min, robot.getx());
    }

    @Test
    public void shouldRobotRight() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(11, robot.getx());
    }

    @Test
    public void shouldRobotRightAtFail() {
        Robot robot = new Robot("Robot", 'R', Robot.X_max, 10);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_max, robot.getx());
    }

    @Test
    public void shouldRobotRightBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_max - 1, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(Robot.X_max, robot.getx());
    }
    
    
}


