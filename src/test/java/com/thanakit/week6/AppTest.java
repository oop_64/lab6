package com.thanakit.week6;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void depositest()
    {   
        BookBank book = new BookBank("thnakit" , 0);
        boolean result = book.deposit(50);
        assertEquals(true, result);
        assertEquals(50, book.getbalance(), 0.00001);
    }

    @Test
    public void depositestNagative()
    {   
        BookBank book = new BookBank("thnakit" , 0);
        boolean result = book.deposit(-50);
        assertEquals(false, result);
        assertEquals(0, book.getbalance(), 0.00001);
    }

    @Test
    public void withdrawtest()
    {   
        BookBank book = new BookBank("thnakit" , 50);
        
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(0, book.getbalance(), 0.00001);
    }


    @Test
    public void withdrawNagativetest()
    {   
        BookBank book = new BookBank("thnakit" , 50);
        
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(50, book.getbalance(), 0.00001);
    }

    @Test
    public void withdrawOver()
    {   
        BookBank book = new BookBank("thnakit" , 50);
        
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(50, book.getbalance(), 0.00001);
    }
}
