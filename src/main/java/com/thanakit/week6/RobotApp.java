package com.thanakit.week6;

public class RobotApp {
    public static void main(String[] args){
        Robot body = new Robot("DoubleBE",'B', 1, 0);
        Robot test = new Robot("TEST",'T', 5, 6);
        body.right(5);
        
        for(int y=Robot.Y_min; y<=Robot.Y_max; y++){
            for(int x=Robot.X_min; x<=Robot.X_max;x++){
                if(body.getx() == x && body.gety() == y){
                    System.out.print(body.getsymbol());
                } else if (test.getx() == x && test.gety() == y){
                    System.out.print(test.getsymbol());
                } else {

                
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
