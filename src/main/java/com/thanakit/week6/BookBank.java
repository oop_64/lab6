package com.thanakit.week6;

public class BookBank {
    private String name;
    private double balance;
    public BookBank(String name , double balance){
        this.name = name;
        this.balance = balance;
    }

    public boolean deposit(double money){
        if(money <= 0){
            return false;
        }
        balance = balance+money;
        return true;
    }

    public boolean withdraw(double money){
        if(money <= 0){
            return false;
        }
        if(money > balance){
            return false;
        }
        balance = balance-money;
        
        return true;
    }

    public void print(){
        System.out.println(name +" " + balance);
    }
    public String getname(){
        return name;
    }
    public double getbalance(){
        return balance;
    }
}
