package com.thanakit.week6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public final static int X_min = 0;
    public final static int X_max = 19;
    public final static int Y_min = 0;
    public final static int Y_max = 19;

    public Robot(String name , char symbol , int x , int y ){
        this.name = name ;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public Robot(String name , char symbol){
        this(name, symbol, 0, 0);
    }

    public void print(){
        System.out.println("Name:" + name + " Symbol:" + symbol + " X:" + x +" Y:" + y);
    }

    public String getname(){
        return name;
    }

    public char getsymbol(){
        return symbol;

    }

    public int getx(){
        return x;

    }

    public int gety(){
        return y;
    }
    
    public boolean right(){
        if(x == X_max){
            return false;
        }
        x = x+1;
        return true;

    }
    public boolean right(int step){
        for(int i=0; i<step; i++){
            if(right() == false){
                return false;
            }
        }

        return true;
    }
    
    public boolean left(){
        if(x == X_min){
            return false;
        }
        x = x-1;
        return true;

    }
    public boolean left(int step){
        for(int i=0; i<step; i++){
            if(left() == false){
                return false;
            }
        }

        return true;
    }

    public boolean up(){
        if(y == Y_min){
            return false;

        }
        y = y-1;
        return true;
    }

    public boolean up(int step){
        for(int i=0; i<step; i++){
            if(up() == false){
                return false;
            }
        }

        return true;
    }


    public boolean down(){
        if(y == Y_max){
            return false;
        }
        y = y+1;
        return true;
    }
    public boolean down(int step){
        for(int i=0; i<step; i++){
            if(down() == false){
                return false;
            }
        }

        return true;

    }
}
